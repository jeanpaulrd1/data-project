# data-project

Proyecto que integra una pequeña web utilizando la librería de python Flask, CloudSQL como base de datos transaccional y la librería SQLAlchemy para hacer la interacción con la BD.
Creamos una imagen de Docker, la publicamos usando Cloud Build. Publicamos nuestro Docker (Imagen) utilizando Cloud Run , con eso tendremos una Web Serverless.
